package com.example.dynamiclis.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.dynamiclis.databinding.ItemColorBinding

class ColorAdapter : RecyclerView.Adapter<ColorAdapter.ColorViewHolder>() {

    private var colors = mutableListOf<Int>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ColorViewHolder {
        val binding = ItemColorBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ColorViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ColorViewHolder, position: Int) {
        val color = colors[position]
        holder.loadColor(color)
    }

    override fun getItemCount(): Int {
        return colors.size
    }

    fun addColors(colors: List<Int>) {
        this.colors = colors.toMutableList()
    }

    class ColorViewHolder(private val binding: ItemColorBinding) : RecyclerView.ViewHolder(binding.root) {
        fun loadColor(color: Int) {
            binding.ivColor.setBackgroundColor(color)
        }
    }
}



















