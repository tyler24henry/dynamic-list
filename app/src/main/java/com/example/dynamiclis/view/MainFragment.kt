package com.example.dynamiclis.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.GridLayoutManager
import com.example.dynamiclis.adapter.ColorAdapter
import com.example.dynamiclis.databinding.FragmentMainBinding
import com.example.dynamiclis.utils.randomColor
import com.example.dynamiclis.viewmodel.ColorViewModel

class MainFragment : Fragment() {

    private var _binding: FragmentMainBinding? = null
    private val binding get() = _binding!!
    private val colorViewModel by viewModels<ColorViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentMainBinding.inflate(inflater, container, false).also { _binding = it }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.btnFetch.setOnClickListener {
            if (binding.etCount.length() > 0) {
                binding.rvList.visibility = View.VISIBLE

                val count = binding.etCount.text.toString().toInt()
                colorViewModel.getColors(count)
            }
        }

        binding.rvList.layoutManager = GridLayoutManager(context, 3)

        colorViewModel.state.observe(viewLifecycleOwner) { colorState ->
            binding.rvList.adapter = ColorAdapter().apply {addColors(colorViewModel?.state?.value ?: listOf(
                randomColor))}
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}











