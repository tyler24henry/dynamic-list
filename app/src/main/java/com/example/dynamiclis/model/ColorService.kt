package com.example.dynamiclis.model

interface ColorService {
    suspend fun getNumberColors(num: Int) : Int
}