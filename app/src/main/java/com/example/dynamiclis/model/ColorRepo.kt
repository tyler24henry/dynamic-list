package com.example.dynamiclis.model

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

object ColorRepo {
    private val colorService = object : ColorService {
        override suspend fun getNumberColors(num: Int) : Int {
            return num
        }
    }

    suspend fun getNumberColors(num: Int) : Int = withContext(Dispatchers.IO) {
        return@withContext colorService.getNumberColors(num)
    }
}






























