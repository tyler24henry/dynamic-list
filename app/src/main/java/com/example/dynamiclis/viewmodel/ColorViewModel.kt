package com.example.dynamiclis.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.dynamiclis.model.ColorRepo
import com.example.dynamiclis.utils.randomColor
import kotlinx.coroutines.launch

class ColorViewModel() : ViewModel() {
    private val repo by lazy { ColorRepo }
    private val _state = MutableLiveData<List<Int>>()
    val state: LiveData<List<Int>> get() = _state

    fun getColors(num: Int) {
        viewModelScope.launch {
            var numColors = repo.getNumberColors(num)

            var colorsList: MutableList<Int> = mutableListOf()

            while (numColors > 0) {
                colorsList.add(randomColor)
                numColors--
            }

            _state.value = colorsList
        }
    }
}
















